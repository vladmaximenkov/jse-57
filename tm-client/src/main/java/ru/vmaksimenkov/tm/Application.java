package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vmaksimenkov.tm.component.Bootstrap;
import ru.vmaksimenkov.tm.configuration.ClientConfiguration;
import ru.vmaksimenkov.tm.util.SystemUtil;

public final class Application {

    public static void main(@Nullable final String[] args) {
        System.out.println(SystemUtil.getPID());
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        context.registerShutdownHook();
        bootstrap.run(args);
    }

}
