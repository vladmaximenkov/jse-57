package ru.vmaksimenkov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class Constant {

    @NotNull
    public final static String PID_FILENAME = "task-manager.pid";

    @NotNull
    public static final String COMMANDS = "COMMANDS";

    @NotNull
    public static final String COMMANDS_FILE = "./commands.txt";

    @NotNull
    public static final String ERRORS = "ERRORS";

    @NotNull
    public static final String ERRORS_FILE = "./errors.txt";

    @NotNull
    public static final String LOGGER_FILE = "/logger.properties";

    @NotNull
    public static final String MESSAGES = "MESSAGES";

    @NotNull
    public static final String MESSAGES_FILE = "./messages.txt";

    @NotNull
    public static final String EXIT_COMMAND = "exit";

}
