package ru.vmaksimenkov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.endpoint.AdminEndpoint;
import ru.vmaksimenkov.tm.listener.AbstractListener;
import ru.vmaksimenkov.tm.service.SessionService;

public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected SessionService sessionService;

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

}
