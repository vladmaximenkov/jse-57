package ru.vmaksimenkov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

@Component
public final class DataJsonLoadFasterXmlListener extends AbstractDataListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Load json data from file";
    }

    @NotNull
    @Override
    public String command() {
        return "data-json-load";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonLoadFasterXmlListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA JSON LOAD]");
        adminEndpoint.loadJson(sessionService.getSession());
    }

}
