package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class ProjectByIndexViewListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "View project by index";
    }

    @NotNull
    @Override
    public String command() {
        return "project-view-by-index";
    }

    @Override
    @EventListener(condition = "@projectByIndexViewListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        showProject(projectEndpoint.findProjectByIndex(sessionService.getSession(), TerminalUtil.nextNumber()));
    }

}
