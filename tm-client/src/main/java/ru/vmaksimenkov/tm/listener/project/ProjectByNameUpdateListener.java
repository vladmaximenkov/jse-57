package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Component
public final class ProjectByNameUpdateListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by name";
    }

    @NotNull
    @Override
    public String command() {
        return "project-update-by-name";
    }

    @Override
    @EventListener(condition = "@projectByNameUpdateListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        if (!projectEndpoint.existsProjectByName(sessionService.getSession(), name))
            throw new ProjectNotFoundException();
        System.out.println("ENTER NEW NAME:");
        @NotNull final String nameNew = TerminalUtil.nextLine();
        if (isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        projectEndpoint.updateProjectByName(sessionService.getSession(), name, nameNew, TerminalUtil.nextLine());
    }

}
