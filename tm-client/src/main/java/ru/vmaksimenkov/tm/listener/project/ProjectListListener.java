package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.ProjectRecord;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    @EventListener(condition = "@projectListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        out.println("[PROJECT LIST]");
        out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED");
        @NotNull AtomicInteger index = new AtomicInteger(1);
        @Nullable final List<ProjectRecord> list = projectEndpoint.findProjectAll(sessionService.getSession());
        if (list == null) return;
        list.forEach((x) -> out.printf("%-3s | %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n",
                index.getAndIncrement(), x.getId(), x.getStatus(), x.getName(), x.getCreated(), x.getDateStart(), x.getDateFinish())
        );
    }

}
