package ru.vmaksimenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.component.Bootstrap;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.listener.AbstractListener;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Component
public final class CommandsListener extends AbstractListener {

    @NotNull
    @Autowired
    protected Bootstrap bootstrap;
    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Override
    public String argument() {
        return "-c";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all listeners";
    }

    @NotNull
    @Override
    public String command() {
        return "commands";
    }

    @Override
    @EventListener(condition = "@commandsListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (!isEmpty(listener.command())) System.out.println(listener.command());
        }
    }

}
