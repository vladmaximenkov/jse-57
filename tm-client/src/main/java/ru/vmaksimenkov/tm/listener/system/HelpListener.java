package ru.vmaksimenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.listener.AbstractListener;

@Component
public final class HelpListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Override
    public String argument() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all listeners";
    }

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @Override
    @EventListener(condition = "@helpListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener.command() + ": " + listener.description());
        }
    }

}