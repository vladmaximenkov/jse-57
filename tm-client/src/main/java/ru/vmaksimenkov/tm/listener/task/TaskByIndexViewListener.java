package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIndexViewListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "View task by index";
    }

    @NotNull
    @Override
    public String command() {
        return "task-view-by-index";
    }

    @Override
    @EventListener(condition = "@taskByIndexViewListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        showTask(taskEndpoint.findTaskByIndex(sessionService.getSession(), TerminalUtil.nextNumber()));
    }

}
