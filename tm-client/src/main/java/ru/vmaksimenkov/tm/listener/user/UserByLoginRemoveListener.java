package ru.vmaksimenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.AdminUserEndpoint;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

@Component
public final class UserByLoginRemoveListener extends AbstractUserListener {

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login";
    }

    @NotNull
    @Override
    public String command() {
        return "user-remove-by-login";
    }

    @Override
    @EventListener(condition = "@userByLoginRemoveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        adminUserEndpoint.removeUserByLogin(sessionService.getSession(), nextLine());
    }

}
