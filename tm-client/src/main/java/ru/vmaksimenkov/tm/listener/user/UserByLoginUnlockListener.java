package ru.vmaksimenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.AdminUserEndpoint;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

@Component
public final class UserByLoginUnlockListener extends AbstractUserListener {

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user by login";
    }

    @NotNull
    @Override
    public String command() {
        return "user-unlock-by-login";
    }

    @Override
    @EventListener(condition = "@userByLoginUnlockListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        adminUserEndpoint.unlockUserByLogin(sessionService.getSession(), nextLine());
    }

}
