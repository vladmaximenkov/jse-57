package ru.vmaksimenkov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.ISessionService;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;

@Getter
@Setter
@Service
public class SessionService implements ISessionService {

    @Nullable
    private SessionRecord session;

}
