package ru.vmaksimenkov.tm.api.repository.dto;

import ru.vmaksimenkov.tm.dto.SessionRecord;

public interface ISessionRecordRepository extends IAbstractBusinessRecordRepository<SessionRecord> {

}