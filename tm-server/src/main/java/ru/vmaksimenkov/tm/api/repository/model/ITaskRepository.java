package ru.vmaksimenkov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractBusinessRepository<Task> {

    void bindTaskPyProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    boolean existsByName(@Nullable String userId, @Nullable String name);

    boolean existsByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task findByName(@Nullable String userId, @Nullable String name);

    void removeAllBinded(@Nullable String userId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeOneByName(@Nullable String userId, @Nullable String name);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String id);

}
