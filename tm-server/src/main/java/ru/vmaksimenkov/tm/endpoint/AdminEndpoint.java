package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.vmaksimenkov.tm.api.endpoint.IAdminEndpoint;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.component.Backup;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private Backup backup;

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @Override
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @WebMethod
    public void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        backup.loadJson();
    }

    @Override
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        backup.run();
    }

    @Override
    @WebMethod
    public void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        backup.saveJson();
    }

}
