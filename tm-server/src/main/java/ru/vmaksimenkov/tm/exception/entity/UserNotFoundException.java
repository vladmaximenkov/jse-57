package ru.vmaksimenkov.tm.exception.entity;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("Error! UserRecord not found...");
    }

}
