package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IAbstractRecordRepository;
import ru.vmaksimenkov.tm.dto.AbstractEntityRecord;
import ru.vmaksimenkov.tm.repository.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

public abstract class AbstractRecordRepository<E extends AbstractEntityRecord> extends Repository implements IAbstractRecordRepository<E> {

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    public void add(@Nullable final List<E> list) {
        if (list == null) return;
        for (@Nullable final E e : list) {
            add(e);
        }
    }

    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        entityManager.persist(entity);
        return entity;
    }

    protected E getEntity(@NotNull final TypedQuery<E> query) {
        @NotNull final List<E> list = query.getResultList();
        if (list.isEmpty()) return null;
        return list.get(0);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.remove(entity);
    }

}