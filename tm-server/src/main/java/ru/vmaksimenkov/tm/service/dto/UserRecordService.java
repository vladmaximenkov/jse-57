package ru.vmaksimenkov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.empty.EmptyEmailException;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyLoginException;
import ru.vmaksimenkov.tm.exception.empty.EmptyPasswordException;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.EmailExistsException;
import ru.vmaksimenkov.tm.exception.user.LoginExistsException;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;
import ru.vmaksimenkov.tm.util.HashUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
public final class UserRecordService extends AbstractRecordService<UserRecord, UserRecordRepository> implements IUserRecordService {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Override
    protected UserRecordRepository getRepository() {
        return context.getBean(UserRecordRepository.class);
    }

    @NotNull
    @SneakyThrows
    public UserRecord create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (existsByLogin(login)) throw new LoginExistsException();
        if (existsByEmail(email)) throw new EmailExistsException();
        @NotNull final UserRecord user = new UserRecord();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    public UserRecord create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (existsByLogin(login)) throw new LoginExistsException();
        @NotNull final UserRecord user = new UserRecord();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
        return user;
    }

    @SneakyThrows
    public boolean existsByEmail(@Nullable final String email) {
        @NotNull final IUserRecordRepository repository = getRepository();
        try {
            repository.begin();
            return repository.existsByEmail(email);
        } finally {
            repository.close();
        }
    }

    @SneakyThrows
    public boolean existsByLogin(@Nullable final String login) {
        @NotNull final IUserRecordRepository repository = getRepository();
        try {
            repository.begin();
            return repository.existsByLogin(login);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @SneakyThrows
    public UserRecord findByLogin(@Nullable final String login) {
        @NotNull final IUserRecordRepository repository = getRepository();
        try {
            repository.begin();
            return repository.findByLogin(login);
        } finally {
            repository.close();
        }
    }

    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final IUserRecordRepository repository = getRepository();
        try {
            repository.begin();
            @Nullable final UserRecord user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            update(user);
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        remove(findByLogin(login));
    }

    @SneakyThrows
    public void setPassword(@NotNull final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final IUserRecordRepository repository = getRepository();
        try {
            repository.begin();
            @Nullable final UserRecord user = repository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            update(user);
        } finally {
            repository.close();
        }
    }

    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final UserRecord user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @SneakyThrows
    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @Nullable final UserRecord user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        update(user);
    }

}
