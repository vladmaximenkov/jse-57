package ru.vmaksimenkov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.repository.model.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.model.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.model.IProjectTaskService;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.repository.model.ProjectRepository;
import ru.vmaksimenkov.tm.repository.model.TaskRepository;

import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    public ProjectRepository getProjectRepository() {
        return context.getBean(ProjectRepository.class);
    }

    @NotNull
    public TaskRepository getTaskRepository() {
        return context.getBean(TaskRepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            projectRepository.begin();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
            taskRepository.commit();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clearTasks(@NotNull final String userId) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.removeAllBinded(userId);
            taskRepository.commit();
            projectRepository.begin();
            projectRepository.clear(userId);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final ITaskRepository repository = getTaskRepository();
        try {
            repository.begin();
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.remove(projectRepository.findById(userId, projectId));
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
            projectRepository.remove(projectRepository.findByIndex(userId, projectIndex));
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.remove(projectRepository.findByName(userId, projectName));
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.unbindTaskFromProject(userId, taskId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}
