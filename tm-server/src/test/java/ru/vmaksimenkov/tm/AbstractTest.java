package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vmaksimenkov.tm.configuration.ServerConfiguration;
import ru.vmaksimenkov.tm.service.AuthService;
import ru.vmaksimenkov.tm.service.PropertyService;

import java.util.Date;

public class AbstractTest {

    @NotNull
    protected static final AnnotationConfigApplicationContext CONTEXT =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    protected static final PropertyService PROPERTY_SERVICE = CONTEXT.getBean(PropertyService.class);

    @NotNull
    protected static final AuthService AUTH_SERVICE = CONTEXT.getBean(AuthService.class);

    @NotNull
    protected static final Date TEST_DATE = new Date(1212121212121L);

    @NotNull
    protected static final String TEST_DESCRIPTION = "test-description-123";

    @NotNull
    protected static final String TEST_PASSWORD = "test-password";

    @NotNull
    protected static final String TEST_PROJECT_NAME = "ProjectRecord test name";

    @NotNull
    protected static final String TEST_PROJECT_NAME_TWO = "XYZ ProjectRecord test name";

    @NotNull
    protected static final String TEST_TASK_NAME = "TaskRecord test name";

    @NotNull
    protected static final String TEST_TASK_NAME_TWO = "XYZ TaskRecord test name";

    @NotNull
    protected static final String TEST_USER_EMAIL = "test@user.email";

    @NotNull
    protected static final String TEST_USER_NAME = "test_user";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "test_user_pass";

    @NotNull
    protected static String TEST_PROJECT_ID;

    @NotNull
    protected static String TEST_SESSION_ID;

    @NotNull
    protected static String TEST_TASK_ID;

    @NotNull
    protected static String TEST_USER_ID;

}
