package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractRecordTest;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.marker.DBCategory;

import java.util.ArrayList;
import java.util.List;

public class ProjectRecordRepositoryTest extends AbstractRecordTest {

    @Test
    @Category(DBCategory.class)
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, PROJECT_REPOSITORY.findAll().size());
    }

    @After
    public void after() {
        PROJECT_REPOSITORY.clear();
        USER_REPOSITORY.clear();
        PROJECT_REPOSITORY.commit();
        USER_REPOSITORY.commit();
    }

    @Before
    public void before() {
        PROJECT_REPOSITORY.begin();
        USER_REPOSITORY.begin();
        TEST_USER = AUTH_SERVICE.registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final ProjectRecord project = new ProjectRecord();
        TEST_PROJECT_ID = project.getId();
        project.setName(TEST_PROJECT_NAME);
        project.setUserId(TEST_USER_ID);
        PROJECT_REPOSITORY.add(project);
        TEST_PROJECT = PROJECT_REPOSITORY.findById(TEST_USER_ID, project.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        PROJECT_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(PROJECT_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        PROJECT_REPOSITORY.clear();
        Assert.assertTrue(PROJECT_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void create() {
        Assert.assertNotNull(TEST_PROJECT);
        Assert.assertNotNull(TEST_PROJECT.getId());
        Assert.assertNotNull(TEST_PROJECT.getName());
        Assert.assertEquals(TEST_PROJECT_NAME, TEST_PROJECT.getName());
        Assert.assertNotNull(PROJECT_REPOSITORY.findAll(TEST_USER_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsById() {
        Assert.assertTrue(PROJECT_REPOSITORY.existsById(TEST_USER_ID, TEST_PROJECT_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByName() {
        Assert.assertTrue(PROJECT_REPOSITORY.existsByName(TEST_USER_ID, TEST_PROJECT_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        initTwo();
        @NotNull final ProjectRecord project3 = new ProjectRecord(TEST_PROJECT_NAME);
        project3.setUserId(TEST_USER_ID);
        project3.setDescription(TEST_DESCRIPTION);
        project3.setStatus(Status.IN_PROGRESS);
        project3.setDateFinish(TEST_DATE);
        project3.setName(TEST_PROJECT_NAME_TWO + TEST_PROJECT_NAME);
        PROJECT_REPOSITORY.add(project3);
        @Nullable final ProjectRecord projectFromRepo = PROJECT_REPOSITORY.findByIndex(TEST_USER_ID, 3);
        Assert.assertNotNull(projectFromRepo);
        Assert.assertEquals(TEST_DESCRIPTION, projectFromRepo.getDescription());
        Assert.assertEquals(TEST_DATE, projectFromRepo.getDateFinish());
        Assert.assertEquals(Status.IN_PROGRESS, projectFromRepo.getStatus());
        Assert.assertEquals(TEST_PROJECT_NAME_TWO + TEST_PROJECT_NAME, projectFromRepo.getName());
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        Assert.assertEquals(TEST_PROJECT, PROJECT_REPOSITORY.findByName(TEST_USER_ID, TEST_PROJECT_NAME));
        Assert.assertEquals(TEST_PROJECT_ID, PROJECT_REPOSITORY.getIdByName(TEST_USER_ID, TEST_PROJECT_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void getIdByIndex() {
        initTwo();
        @NotNull final ProjectRecord project3 = new ProjectRecord(TEST_PROJECT_NAME);
        project3.setUserId(TEST_USER_ID);
        PROJECT_REPOSITORY.add(project3);
        Assert.assertEquals(project3.getId(), PROJECT_REPOSITORY.getIdByIndex(TEST_USER_ID, 3));
    }

    private void initTwo() {
        PROJECT_REPOSITORY.clear(TEST_USER_ID);
        @NotNull final ProjectRecord project1 = new ProjectRecord(TEST_PROJECT_NAME);
        project1.setUserId(TEST_USER_ID);
        project1.setStatus(Status.IN_PROGRESS);
        @NotNull final ProjectRecord project2 = new ProjectRecord(TEST_PROJECT_NAME_TWO);
        project2.setUserId(TEST_USER_ID);
        project2.setStatus(Status.COMPLETE);
        @NotNull final List<ProjectRecord> list = new ArrayList<>();
        list.add(project1);
        list.add(project2);
        PROJECT_REPOSITORY.add(list);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        Assert.assertNotNull(TEST_PROJECT);
        PROJECT_REPOSITORY.remove(TEST_PROJECT);
        Assert.assertNull(PROJECT_REPOSITORY.findById(TEST_PROJECT.getId()));
        Assert.assertTrue(PROJECT_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByName() {
        initTwo();
        PROJECT_REPOSITORY.removeByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertEquals(TEST_PROJECT_NAME_TWO, PROJECT_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

}
