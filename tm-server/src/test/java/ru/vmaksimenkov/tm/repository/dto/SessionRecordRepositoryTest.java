package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractRecordTest;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.marker.DBCategory;

public class SessionRecordRepositoryTest extends AbstractRecordTest {

    @After
    public void after() {
        SESSION_REPOSITORY.clear();
        USER_REPOSITORY.clear();
        SESSION_REPOSITORY.commit();
        USER_REPOSITORY.commit();
    }

    @Before
    @Category(DBCategory.class)
    public void before() {
        SESSION_REPOSITORY.begin();
        USER_REPOSITORY.begin();
        TEST_USER = AUTH_SERVICE.registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final SessionRecord session = new SessionRecord();
        TEST_SESSION_ID = session.getId();
        session.setUserId(TEST_USER_ID);
        SESSION_REPOSITORY.add(session);
        TEST_SESSION = SESSION_REPOSITORY.findById(TEST_SESSION_ID);
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        SESSION_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(SESSION_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        SESSION_REPOSITORY.clear();
        Assert.assertTrue(SESSION_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findByUserId() {
        Assert.assertFalse(SESSION_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        Assert.assertEquals(SESSION_REPOSITORY.findAll(TEST_USER_ID).get(0), TEST_SESSION);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByUserId() {
        SESSION_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertEquals(0, (long) SESSION_REPOSITORY.size(TEST_USER_ID));
    }
}
