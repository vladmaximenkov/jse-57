package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractRecordTest;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.marker.DBCategory;

import java.util.ArrayList;
import java.util.List;

public class TaskRecordRepositoryTest extends AbstractRecordTest {

    @After
    public void after() {
        TASK_REPOSITORY.clear();
        USER_REPOSITORY.clear();
        TASK_REPOSITORY.commit();
        USER_REPOSITORY.commit();
    }

    @Before
    public void before() {
        TASK_REPOSITORY.begin();
        USER_REPOSITORY.begin();
        TEST_USER = AUTH_SERVICE.registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        TEST_TASK = new TaskRecord();
        TEST_TASK_ID = TEST_TASK.getId();
        TEST_TASK.setName(TEST_TASK_NAME);
        TEST_TASK.setUserId(TEST_USER_ID);
        TASK_REPOSITORY.add(TEST_TASK);
        TEST_TASK = TASK_REPOSITORY.findById(TEST_USER_ID, TEST_TASK.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        TASK_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(TASK_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        TASK_REPOSITORY.clear();
        Assert.assertTrue(TASK_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void create() {
        Assert.assertNotNull(TEST_TASK);
        Assert.assertNotNull(TEST_TASK.getId());
        Assert.assertNotNull(TEST_TASK.getName());
        Assert.assertEquals(TEST_TASK_NAME, TEST_TASK.getName());
        Assert.assertNotNull(TASK_REPOSITORY.findAll(TEST_USER_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsById() {
        Assert.assertTrue(TASK_REPOSITORY.existsById(TEST_USER_ID, TEST_TASK_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByName() {
        Assert.assertTrue(TASK_REPOSITORY.existsByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        initTwo();
        @NotNull final TaskRecord task3 = new TaskRecord(TEST_TASK_NAME);
        task3.setUserId(TEST_USER_ID);
        task3.setDescription(TEST_DESCRIPTION);
        task3.setStatus(Status.IN_PROGRESS);
        task3.setDateFinish(TEST_DATE);
        task3.setName(TEST_TASK_NAME_TWO + TEST_TASK_NAME);
        TASK_REPOSITORY.add(task3);
        @Nullable final TaskRecord taskFromRepo = TASK_REPOSITORY.findByIndex(TEST_USER_ID, 3);
        Assert.assertNotNull(taskFromRepo);
        Assert.assertEquals(TEST_DESCRIPTION, taskFromRepo.getDescription());
        Assert.assertEquals(TEST_DATE, taskFromRepo.getDateFinish());
        Assert.assertEquals(Status.IN_PROGRESS, taskFromRepo.getStatus());
        Assert.assertEquals(TEST_TASK_NAME_TWO + TEST_TASK_NAME, taskFromRepo.getName());
    }

    private void initTwo() {
        TASK_REPOSITORY.clear(TEST_USER_ID);
        @NotNull final TaskRecord task1 = new TaskRecord(TEST_TASK_NAME);
        task1.setUserId(TEST_USER_ID);
        task1.setStatus(Status.IN_PROGRESS);
        @NotNull final TaskRecord task2 = new TaskRecord(TEST_TASK_NAME_TWO);
        task2.setUserId(TEST_USER_ID);
        task2.setStatus(Status.COMPLETE);
        @NotNull final List<TaskRecord> list = new ArrayList<>();
        list.add(task1);
        list.add(task2);
        TASK_REPOSITORY.add(list);
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        Assert.assertEquals(TEST_TASK, TASK_REPOSITORY.findByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void getIdByIndex() {
        initTwo();
        @NotNull final TaskRecord task3 = new TaskRecord(TEST_TASK_NAME);
        task3.setUserId(TEST_USER_ID);
        TASK_REPOSITORY.add(task3);
        Assert.assertEquals(task3.getId(), TASK_REPOSITORY.getIdByIndex(TEST_USER_ID, 3));
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        Assert.assertNotNull(TEST_TASK);
        TASK_REPOSITORY.remove(TEST_TASK);
        Assert.assertNull(TASK_REPOSITORY.findById(TEST_TASK.getId()));
        Assert.assertTrue(TASK_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByName() {
        initTwo();
        TASK_REPOSITORY.removeOneByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

}
