package ru.vmaksimenkov.tm.service.dto;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractRecordTest;
import ru.vmaksimenkov.tm.marker.DBCategory;

public class UserRecordServiceTest extends AbstractRecordTest {

    @After
    public void after() {
        USER_SERVICE.clear();
    }

    @Before
    public void before() {
        TEST_USER = AUTH_SERVICE.registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
    }

    @Test
    @Category(DBCategory.class)
    public void existByEmail() {
        Assert.assertTrue(USER_SERVICE.existsByEmail(TEST_USER_EMAIL));
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        Assert.assertEquals(TEST_USER_ID, USER_SERVICE.findByLogin(TEST_USER_NAME).getId());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        USER_SERVICE.removeByLogin(TEST_USER_NAME);
        Assert.assertTrue(USER_SERVICE.findAll().isEmpty());
    }

}
